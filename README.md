# Return Key API

## Tech Stack

- Java 17
- Spring Boot 2.7
- H2 Database

## Getting Started

1. Clone this repo 
2. Make sure JDK 17 is installed in your machine
3. Open the project on IDE of your choice (I use IntelliJ IDEA CE)
4. Debug/Run in ReturnKeyApplication.java
5. Access API at http://localhost:8080

## Order Data
There will be order data pre-loaded from order.csv given. The file put under src/main/resources/file/order.csv.

## API Flow

### Generate Return Token

Request
```bash
curl --request POST \
  --url 'http://localhost:8080/pending/returns?=' \
  --header 'Content-Type: application/json' \
  --data '{
	"orderId": "RK-238",
	"customerEmail": "carly@example.com"
}'
```

Response 200
```json
{
  "token": "15993980-c5b8-4545-a870-53098c063e2c"
}
```

Response 404 Not Found
```json
{
  "message": "Not a valid order.", 
  "fields": null
}
```

### Create Return
Request
```bash
curl --request POST \
  --url http://localhost:8080/returns \
  --header 'Content-Type: application/json' \
  --data '{
	"token": "15993980-c5b8-4545-a870-53098c063e2c"
}'
```

Response 200
```json
{
  "id": 1, 
  "estimatedRefundAmount": 113.6
}
```

### Get Return
Request
```shell
curl --request GET \
  --url http://localhost:8080/returns/1
```

Response 200
```json
{
  "id": 2,
  "status": "AWAITING_APPROVAL",
  "estimatedAmount": 160.75,
  "orderId": "RK-478",
  "customerEmail": "john@example.com",
  "returnItems": [
    {
      "id": 4,
      "status": "PENDING",
      "acceptedQuantity": 0,
      "orderItemId": 1,
      "sku": "MENS-156",
      "itemName": "Small Black T-Shirt",
      "price": 50.0
    },
    {
      "id": 5,
      "status": "PENDING",
      "acceptedQuantity": 0,
      "orderItemId": 2,
      "sku": "NIKE-7",
      "itemName": "Nike Air Jordans - Size 7",
      "price": 110.75
    }
  ]
}
```

### Update Return Item QC Status
Request
```bash
curl --request PUT \
  --url http://localhost:8080/returns/1/items/3/qc/status \
  --header 'Content-Type: application/json' \
  --data '{
	"status": "REJECTED"
}'
```

Response 200
```json
{
	"id": 3,
	"status": "REJECTED",
	"acceptedQuantity": 2,
	"orderItemId": 6,
	"sku": "MU-4129",
	"itemName": "Eye Shadow",
	"price": 22.85
}
```

Notes: when all return item is accepted/rejected, return status will be `COMPLETE` and the estimatedAmount will return the accurate amount