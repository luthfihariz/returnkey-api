package com.luthfihariz.returnkeyapi.application.returns.port;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class GetReturnByIdResponse {
    private Long id;
    private ReturnState status;
    private Double estimatedAmount;
    private String orderId;
    private String customerEmail;
    private List<GetReturnItemResponse> returnItems;
}
