package com.luthfihariz.returnkeyapi.application.returns.api;


import com.luthfihariz.returnkeyapi.application.returns.port.PendingReturnsRequest;
import com.luthfihariz.returnkeyapi.application.returns.port.PendingReturnsResponse;
import com.luthfihariz.returnkeyapi.core.returns.service.ReturnTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pending/returns")
public class PendingReturnApi {
    @Autowired
    private ReturnTokenService returnTokenService;

    @RequestMapping(
            value = {""},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<PendingReturnsResponse> post(
            @RequestBody PendingReturnsRequest request
    ) throws Exception {
        String token = returnTokenService.generate(
                request.getOrderId(), request.getCustomerEmail()
        );
        return new ResponseEntity<>(new PendingReturnsResponse(token), HttpStatus.OK);
    }
}