package com.luthfihariz.returnkeyapi.application.returns.port;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class PendingReturnsResponse {
    private String token;
}
