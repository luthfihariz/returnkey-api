package com.luthfihariz.returnkeyapi.application.returns.port;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class GetReturnItemResponse {
    private Long id;
    private ReturnItemState status;
    private int acceptedQuantity;
    private Long orderItemId;
    private String sku;
    private String itemName;
    private Double price;
}
