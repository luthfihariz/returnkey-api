package com.luthfihariz.returnkeyapi.application.returns.port;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateReturnItemStatusRequest {
    private ReturnItemState status;
}
