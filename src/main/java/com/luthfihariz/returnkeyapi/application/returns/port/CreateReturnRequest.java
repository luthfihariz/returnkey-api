package com.luthfihariz.returnkeyapi.application.returns.port;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateReturnRequest {
    private String token;
}
