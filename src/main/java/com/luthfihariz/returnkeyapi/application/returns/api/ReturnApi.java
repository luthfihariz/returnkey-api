package com.luthfihariz.returnkeyapi.application.returns.api;


import com.luthfihariz.returnkeyapi.application.returns.port.*;
import com.luthfihariz.returnkeyapi.core.order.port.UpdateReturnItemStatusSpec;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnDomain;
import com.luthfihariz.returnkeyapi.core.returns.service.ReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/returns")
public class ReturnApi {

    @Autowired
    private ReturnService returnService;

    @RequestMapping(
            value = {""},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<CreateReturnResponse> createReturn(
            @RequestBody CreateReturnRequest request
    ) throws Exception {
        ReturnDomain returnDomain = returnService.create(request.getToken());
        return new ResponseEntity<>(new CreateReturnResponse(
                returnDomain.getId(), returnDomain.getEstimatedAmount()
        ), HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/{id}"},
            method = {RequestMethod.GET}
    )
    public ResponseEntity<GetReturnByIdResponse> getById(
            @PathVariable("id") Long id
    ) throws Exception {
        return new ResponseEntity<>(returnService.getById(id), HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/{id}/items/{itemId}/qc/status"},
            method = {RequestMethod.PUT}
    )
    public ResponseEntity<GetReturnItemResponse> updateReturnItemStatus(
            @PathVariable("id") Long returnId, @PathVariable("itemId") Long returnItemId,
            @RequestBody UpdateReturnItemStatusRequest request
    ) throws Exception {
        return new ResponseEntity<>(returnService.updateReturnItemStatus(
                new UpdateReturnItemStatusSpec(returnId, returnItemId, request.getStatus())
        ), HttpStatus.OK);
    }
}
