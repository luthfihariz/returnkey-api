package com.luthfihariz.returnkeyapi.application.returns.port;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateReturnResponse {
    private Long id;
    private Double estimatedRefundAmount;
}
