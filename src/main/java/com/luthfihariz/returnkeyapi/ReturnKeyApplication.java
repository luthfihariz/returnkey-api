package com.luthfihariz.returnkeyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "com.luthfihariz.returnkeyapi"
})
public class ReturnKeyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReturnKeyApplication.class, args);
    }

}
