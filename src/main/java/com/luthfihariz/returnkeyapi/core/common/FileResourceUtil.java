package com.luthfihariz.returnkeyapi.core.common;

import java.io.InputStream;

public class FileResourceUtil {
    public InputStream getFileUriFromResourceAsStream(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        if (inputStream == null) {
            throw new IllegalArgumentException("File not found: " + fileName);
        }

        return inputStream;
    }
}
