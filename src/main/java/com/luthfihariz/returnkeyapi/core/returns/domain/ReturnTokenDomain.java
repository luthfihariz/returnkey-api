package com.luthfihariz.returnkeyapi.core.returns.domain;

import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnTokenDomain {
    private Long id;
    private String token;
    private Date expiredAt;
    private OrderDomain order;
}
