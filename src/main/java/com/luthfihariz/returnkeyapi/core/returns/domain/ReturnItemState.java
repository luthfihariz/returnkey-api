package com.luthfihariz.returnkeyapi.core.returns.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ReturnItemState {
    ACCEPTED("ACCEPTED"), REJECTED("REJECTED"), PENDING("PENDING");

    private final String value;

    ReturnItemState(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
