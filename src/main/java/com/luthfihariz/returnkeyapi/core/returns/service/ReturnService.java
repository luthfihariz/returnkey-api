package com.luthfihariz.returnkeyapi.core.returns.service;

import com.luthfihariz.returnkeyapi.application.returns.port.GetReturnByIdResponse;
import com.luthfihariz.returnkeyapi.application.returns.port.GetReturnItemResponse;
import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderItemAccessor;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;
import com.luthfihariz.returnkeyapi.core.order.port.UpdateReturnItemStatusSpec;
import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnAccessor;
import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnItemAccessor;
import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnTokenAccessor;
import com.luthfihariz.returnkeyapi.core.returns.domain.*;
import com.luthfihariz.returnkeyapi.infrastructure.common.exception.BadRequestException;
import com.luthfihariz.returnkeyapi.infrastructure.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReturnService {

    @Autowired
    private IReturnAccessor returnAccessor;

    @Autowired
    private IReturnTokenAccessor returnTokenAccessor;

    @Autowired
    private IOrderItemAccessor orderItemAccessor;

    @Autowired
    private IReturnItemAccessor returnItemAccessor;

    public ReturnDomain create(String token) throws Exception {
        ReturnTokenDomain returnTokenDomain = returnTokenAccessor.getActiveToken(token);
        if (returnTokenDomain == null) {
            throw new NotFoundException("Token is not valid");
        }
        OrderDomain orderDomain = returnTokenDomain.getOrder();
        ReturnDomain existingRefund = returnAccessor.getByOrderId(orderDomain.getId());
        if (existingRefund != null) {
            throw new BadRequestException("Refund already existed. Refund ID=" + existingRefund.getId());
        }
        List<OrderItemDomain> orderItems = orderItemAccessor.getByOrderId(
                orderDomain.getId()
        );

        ReturnDomain returnDomain = returnAccessor.create(new ReturnDomain(
                null, orderDomain, orderDomain.getTotalPrice(), ReturnState.AWAITING_APPROVAL
        ));

        List<ReturnItemDomain> returnItemDomains = orderItems.stream().map(orderItemDomain -> {
            return new ReturnItemDomain(
                    null, returnDomain, orderItemDomain, 0, ReturnItemState.PENDING
            );
        }).toList();

        returnItemAccessor.bulkCreate(returnItemDomains);
        return returnDomain;
    }

    public GetReturnByIdResponse getById(Long id) throws Exception {
        ReturnDomain returnDomain = returnAccessor.getById(id);
        if (returnDomain == null) {
            throw new NotFoundException("Can't found Return with that id");
        }
        List<ReturnItemDomain> returnItemDomains = returnItemAccessor.getNonRejectedByReturnId(
                returnDomain.getId()
        );

        return new GetReturnByIdResponse(
                returnDomain.getId(),
                returnDomain.getStatus(),
                returnDomain.getEstimatedAmount(),
                returnDomain.getOrder().getId(),
                returnDomain.getOrder().getCustomerEmail(),
                returnItemDomains.stream().map(returnItemDomain -> {
                    return new GetReturnItemResponse(
                            returnItemDomain.getId(),
                            returnItemDomain.getStatus(),
                            returnItemDomain.getAcceptedQuantity(),
                            returnItemDomain.getOrderItem().getId(),
                            returnItemDomain.getOrderItem().getSku(),
                            returnItemDomain.getOrderItem().getItemName(),
                            returnItemDomain.getOrderItem().getPrice()
                    );
                }).toList()
        );
    }

    public GetReturnItemResponse updateReturnItemStatus(UpdateReturnItemStatusSpec spec) throws Exception {
        ReturnItemDomain returnItemDomain = returnItemAccessor.getById(spec.getReturnItemId());
        if (returnItemDomain == null) {
            throw new NotFoundException("Return item not found");
        }

        returnItemDomain = returnItemAccessor.updateStatusAndAcceptedQuantity(
                spec.getReturnItemId(), spec.getStatus(), returnItemDomain.getOrderItem().getQuantity()
        );

        List<ReturnItemDomain> returnItemDomainList = returnItemAccessor.getByReturnId(spec.getReturnId());
        List<ReturnItemDomain> pendingReturnItems = returnItemDomainList.stream().filter(returnItem -> {
            return returnItem.getStatus() == ReturnItemState.PENDING;
        }).toList();
        if (pendingReturnItems.isEmpty()) {
            Double totalReturnedAmount = returnItemDomainList.stream().filter(returnItem -> {
                return returnItem.getStatus() == ReturnItemState.ACCEPTED;
            }).mapToDouble(returnItem -> returnItem.getOrderItem().getPrice()).sum();
            returnAccessor.updateStatusAndEstimatedPrice(spec.getReturnId(), ReturnState.COMPLETE, totalReturnedAmount);
        }

        return new GetReturnItemResponse(returnItemDomain.getId(),
                returnItemDomain.getStatus(),
                returnItemDomain.getAcceptedQuantity(),
                returnItemDomain.getOrderItem().getId(),
                returnItemDomain.getOrderItem().getSku(),
                returnItemDomain.getOrderItem().getItemName(),
                returnItemDomain.getOrderItem().getPrice()
        );
    }
}
