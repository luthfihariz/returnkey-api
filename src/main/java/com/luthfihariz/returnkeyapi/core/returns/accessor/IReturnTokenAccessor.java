package com.luthfihariz.returnkeyapi.core.returns.accessor;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnTokenDomain;

public interface IReturnTokenAccessor {
    public ReturnTokenDomain create(ReturnTokenDomain returnTokenDomain);

    public ReturnTokenDomain getActiveToken(String token) throws Exception;
}
