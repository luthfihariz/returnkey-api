package com.luthfihariz.returnkeyapi.core.returns.accessor;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnState;

public interface IReturnAccessor {
    public ReturnDomain create(ReturnDomain returnDomain) throws Exception;

    public ReturnDomain getById(Long id) throws Exception;

    public ReturnDomain getByOrderId(String orderId) throws Exception;

    public ReturnDomain updateStatusAndEstimatedPrice(Long id, ReturnState status, Double price) throws Exception;
}
