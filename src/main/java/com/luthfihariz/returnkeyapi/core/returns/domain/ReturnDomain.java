package com.luthfihariz.returnkeyapi.core.returns.domain;

import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnDomain {
    private Long id;
    private OrderDomain order;
    private Double estimatedAmount;
    private ReturnState status;
}
