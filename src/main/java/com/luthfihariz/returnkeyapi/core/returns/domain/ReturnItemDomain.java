package com.luthfihariz.returnkeyapi.core.returns.domain;


import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnItemDomain {
    private Long id;
    private ReturnDomain returns;
    private OrderItemDomain orderItem;
    private Integer acceptedQuantity;
    private ReturnItemState status;
}
