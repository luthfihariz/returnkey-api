package com.luthfihariz.returnkeyapi.core.returns.service;

import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderAccessor;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnAccessor;
import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnTokenAccessor;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnTokenDomain;
import com.luthfihariz.returnkeyapi.infrastructure.common.exception.BadRequestException;
import com.luthfihariz.returnkeyapi.infrastructure.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
public class ReturnTokenService {

    private final IOrderAccessor orderAccessor;
    private final IReturnAccessor returnAccessor;
    private final IReturnTokenAccessor returnTokenAccessor;

    @Autowired
    public ReturnTokenService(IOrderAccessor orderAccessor, IReturnTokenAccessor returnTokenAccessor, IReturnAccessor returnAccessor) {
        this.orderAccessor = orderAccessor;
        this.returnTokenAccessor = returnTokenAccessor;
        this.returnAccessor = returnAccessor;
    }

    public String generate(String id, String customerEmail) throws Exception {
        OrderDomain existingOrder = orderAccessor.getByIdAndCustomerEmail(id, customerEmail);

        if (existingOrder == null) {
            throw new NotFoundException("Not a valid order.");
        }

        ReturnDomain existingRefund = returnAccessor.getByOrderId(existingOrder.getId());
        if (existingRefund != null) {
            throw new BadRequestException("Refund already existed. Refund ID="+existingRefund.getId());
        }

        String token = UUID.randomUUID().toString();
        Calendar expiredAt = Calendar.getInstance();
        expiredAt.setTime(new Date());
        expiredAt.add(Calendar.HOUR, 2);

        ReturnTokenDomain returnTokenDomain = new ReturnTokenDomain();
        returnTokenDomain.setToken(token);
        returnTokenDomain.setExpiredAt(expiredAt.getTime());
        returnTokenDomain.setOrder(existingOrder);

        returnTokenAccessor.create(returnTokenDomain);

        return token;
    }
}
