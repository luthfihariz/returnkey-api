package com.luthfihariz.returnkeyapi.core.returns.accessor;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemState;

import java.util.List;

public interface IReturnItemAccessor {
    public ReturnItemDomain getById(Long id) throws Exception;

    public void bulkCreate(List<ReturnItemDomain> returnItemDomains) throws Exception;

    public List<ReturnItemDomain> getNonRejectedByReturnId(Long returnId) throws Exception;

    public List<ReturnItemDomain> getByReturnId(Long returnId) throws Exception;

    public ReturnItemDomain updateStatusAndAcceptedQuantity(Long id, ReturnItemState returnItemState,
                                                            Integer acceptedQuantity) throws Exception;
}
