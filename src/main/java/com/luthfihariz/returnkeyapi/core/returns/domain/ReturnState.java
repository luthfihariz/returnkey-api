package com.luthfihariz.returnkeyapi.core.returns.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ReturnState {
    AWAITING_APPROVAL("AWAITING_APPROVAL"), COMPLETE("COMPLETE");

    private final String value;

    ReturnState(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
