package com.luthfihariz.returnkeyapi.core.order.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class OrderDomain {
    private String id;
    private String customerEmail;
    private Double totalPrice;
}
