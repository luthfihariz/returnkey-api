package com.luthfihariz.returnkeyapi.core.order.port;

import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetOrderByIdAndEmailResult {
    private String id;
    private String customerEmail;
    private List<OrderItemDomain> orderItems;
}
