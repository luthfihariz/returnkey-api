package com.luthfihariz.returnkeyapi.core.order.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemDomain {
    private Long id;
    private OrderDomain order;
    private String sku;
    private String itemName;
    private Integer quantity;
    private Double price;
}
