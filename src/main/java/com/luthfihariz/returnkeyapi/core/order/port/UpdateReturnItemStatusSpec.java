package com.luthfihariz.returnkeyapi.core.order.port;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateReturnItemStatusSpec {
    private Long returnId;
    private Long returnItemId;
    private ReturnItemState status;
}
