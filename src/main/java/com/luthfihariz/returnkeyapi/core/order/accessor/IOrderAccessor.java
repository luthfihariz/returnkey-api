package com.luthfihariz.returnkeyapi.core.order.accessor;

import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;

public interface IOrderAccessor {
    public OrderDomain getByIdAndCustomerEmail(String id, String email) throws Exception;

    public OrderDomain create(OrderDomain orderDomain) throws Exception;
}
