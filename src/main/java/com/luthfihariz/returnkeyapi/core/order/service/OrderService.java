package com.luthfihariz.returnkeyapi.core.order.service;

import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderAccessor;
import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderItemAccessor;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;
import com.luthfihariz.returnkeyapi.core.order.port.GetOrderByIdAndEmailResult;
import com.luthfihariz.returnkeyapi.core.order.port.GetOrderByIdAndEmailSpec;
import com.luthfihariz.returnkeyapi.core.order.port.ValidateOrderSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    private final IOrderAccessor orderAccessor;
    private final IOrderItemAccessor orderItemAccessor;

    @Autowired
    public OrderService(IOrderAccessor orderAccessor, IOrderItemAccessor orderItemAccessor) {
        this.orderAccessor = orderAccessor;
        this.orderItemAccessor = orderItemAccessor;
    }

    public Boolean validateOrder(ValidateOrderSpec spec) throws Exception {
        return orderAccessor.getByIdAndCustomerEmail(spec.getId(), spec.getCustomerEmail()) != null;
    }

    public GetOrderByIdAndEmailResult getOrderByIdAndEmail(GetOrderByIdAndEmailSpec spec) throws Exception {
        OrderDomain order = orderAccessor.getByIdAndCustomerEmail(spec.getId(), spec.getCustomerEmail());
        List<OrderItemDomain> orderItems = orderItemAccessor.getByOrderId(order.getId());
        return new GetOrderByIdAndEmailResult(
                order.getId(),
                order.getCustomerEmail(),
                orderItems
        );
    }
}
