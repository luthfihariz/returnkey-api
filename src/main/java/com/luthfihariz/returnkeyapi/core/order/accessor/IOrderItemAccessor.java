package com.luthfihariz.returnkeyapi.core.order.accessor;

import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;

import java.util.List;

public interface IOrderItemAccessor {
    public List<OrderItemDomain> getByOrderId(String orderId) throws Exception;

    public void bulkCreate(List<OrderItemDomain> orderItemDomains) throws Exception;

}
