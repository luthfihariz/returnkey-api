package com.luthfihariz.returnkeyapi.core.order.service;

import com.luthfihariz.returnkeyapi.core.common.FileResourceUtil;
import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderAccessor;
import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderItemAccessor;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PreLoadOrderService implements InitializingBean {

    @Autowired
    IOrderAccessor orderAccessor;

    @Autowired
    IOrderItemAccessor orderItemAccessor;

    public void loadOrders() throws Exception {
        FileResourceUtil fileResourceUtil = new FileResourceUtil();
        String fileName = "files/orders.csv";
        String line = "";
        String splitBy = ",";

        InputStream is = fileResourceUtil.getFileUriFromResourceAsStream(fileName);
        List<List<String>> dataFromCsv = new ArrayList<>();
        try (InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(streamReader)) {

            boolean isFirstRow = true;
            while ((line = reader.readLine()) != null) {
                if (!StringUtils.hasText(line) || isFirstRow) {
                    isFirstRow = false;
                    continue;
                }

                String[] data = line.split(splitBy);
                dataFromCsv.add(Arrays.asList(data));
            }
        }

        String orderId = "";
        OrderDomain orderDomain = null;
        double totalPrice = 0.0;
        List<OrderItemDomain> orderItems = new ArrayList<>();
        for (List<String> data : dataFromCsv) {
            String emailAddress = data.get(1);
            String sku = data.get(2);
            Integer quantity = Integer.parseInt(data.get(3));
            double price = Double.parseDouble(data.get(4));
            String itemName = data.get(5);

            if (!data.get(0).equals(orderId)) {
                if (orderDomain != null) {
                    orderDomain.setTotalPrice(totalPrice);
                    orderDomain = orderAccessor.create(orderDomain);
                    orderItemAccessor.bulkCreate(orderItems);
                    totalPrice = 0.0;
                    orderItems = new ArrayList<>();
                }

                orderId = data.get(0);
                orderDomain = new OrderDomain();
                orderDomain.setId(orderId);
                orderDomain.setCustomerEmail(emailAddress);
            }

            OrderItemDomain orderItemDomain = new OrderItemDomain();
            orderItemDomain.setOrder(orderDomain);
            orderItemDomain.setItemName(itemName);
            orderItemDomain.setQuantity(quantity);
            orderItemDomain.setSku(sku);
            orderItemDomain.setPrice(price);
            orderItems.add(orderItemDomain);

            totalPrice += price;
        }

        if (orderDomain != null) {
            orderDomain.setTotalPrice(totalPrice);
            orderDomain = orderAccessor.create(orderDomain);
            orderItemAccessor.bulkCreate(orderItems);
        }

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        loadOrders();
    }
}
