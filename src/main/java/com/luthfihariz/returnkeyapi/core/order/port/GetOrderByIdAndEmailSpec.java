package com.luthfihariz.returnkeyapi.core.order.port;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetOrderByIdAndEmailSpec {
    private String id;
    private String customerEmail;
}
