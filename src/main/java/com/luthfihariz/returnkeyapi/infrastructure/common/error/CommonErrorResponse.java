package com.luthfihariz.returnkeyapi.infrastructure.common.error;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CommonErrorResponse {
  private String message;
  private List<String> fields = new ArrayList<>();
}
