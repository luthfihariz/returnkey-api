package com.luthfihariz.returnkeyapi.infrastructure.common.error;

import com.luthfihariz.returnkeyapi.infrastructure.common.exception.BadRequestException;
import com.luthfihariz.returnkeyapi.infrastructure.common.exception.NotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Log4j2
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        CommonErrorResponse commonErrorResponse = new CommonErrorResponse();
        ex.getBindingResult()
                .getAllErrors()
                .forEach(error -> commonErrorResponse.getFields().add(error.getDefaultMessage()));

        log.error("An error occurred", ex);

        return new ResponseEntity<>(commonErrorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Object> handleBindException(
            BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        CommonErrorResponse commonErrorResponse = new CommonErrorResponse();
        ex.getBindingResult()
                .getAllErrors()
                .forEach(error -> commonErrorResponse.getFields().add(error.getDefaultMessage()));

        log.error("An error occurred", ex);

        return new ResponseEntity<>(commonErrorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<Object> handleBadRequest(Exception ex, WebRequest request) {
        CommonErrorResponse commonErrorResponse =
                new CommonErrorResponse(ex.getMessage(), ((BadRequestException) ex).getFieldMessages());

        log.error("An error occurred", ex);

        return new ResponseEntity<>(commonErrorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<CommonErrorResponse> handleNotFound(Exception ex, WebRequest request) {
        CommonErrorResponse commonErrorResponse = new CommonErrorResponse(ex.getMessage(), null);

        log.error("An error occurred", ex);

        return new ResponseEntity<>(commonErrorResponse, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<CommonErrorResponse> handleInternalServerError(
            Exception ex, WebRequest request) {
        CommonErrorResponse commonErrorResponse = new CommonErrorResponse();
        commonErrorResponse.setMessage(ex.getMessage());

        log.error("An error occurred", ex);

        return new ResponseEntity<>(
                commonErrorResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
