package com.luthfihariz.returnkeyapi.infrastructure.order.accessor;

import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderAccessor;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import com.luthfihariz.returnkeyapi.infrastructure.order.entity.Order;
import com.luthfihariz.returnkeyapi.infrastructure.order.mapper.OrderMapper;
import com.luthfihariz.returnkeyapi.infrastructure.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderAccessor implements IOrderAccessor {

    private final OrderRepository repository;

    @Autowired
    public OrderAccessor(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public OrderDomain getByIdAndCustomerEmail(String id, String email) throws Exception {
        Order order = repository.findByIdAndCustomerEmail(id, email);
        return OrderMapper.INSTANCE.orderToOrderDomain(order);
    }

    @Override
    public OrderDomain create(OrderDomain orderDomain) throws Exception {
        Order order = OrderMapper.INSTANCE.orderDomainToOrder(orderDomain);
        order = repository.save(order);
        return OrderMapper.INSTANCE.orderToOrderDomain(order);
    }
}
