package com.luthfihariz.returnkeyapi.infrastructure.order.entity;

import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnItem;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    private String sku;

    private String itemName;

    private Integer quantity;

    private Double price;

    @OneToMany(mappedBy = "orderItem")
    private Set<ReturnItem> returnItems;
}
