package com.luthfihariz.returnkeyapi.infrastructure.order.entity;

import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.Return;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnToken;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "orders", indexes = {@Index(columnList = "id, customerEmail")})
@Data
public class Order {
    @Id
    private String id;

    private String customerEmail;

    private Double totalPrice;

    @OneToMany(mappedBy = "order")
    private Set<ReturnToken> returnTokens;

    @OneToMany(mappedBy = "order")
    private Set<Return> returns;

    @OneToMany(mappedBy = "order")
    private Set<OrderItem> orderItems;
}
