package com.luthfihariz.returnkeyapi.infrastructure.order.mapper;

import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;
import com.luthfihariz.returnkeyapi.infrastructure.order.entity.Order;
import com.luthfihariz.returnkeyapi.infrastructure.order.entity.OrderItem;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderDomain orderToOrderDomain(Order order);

    Order orderDomainToOrder(OrderDomain orderDomain);

    OrderItemDomain orderItemToOrderItemDomain(OrderItem orderItem);

    OrderItem orderItemDomainToOrderItem(OrderItemDomain orderItemDomain);
}
