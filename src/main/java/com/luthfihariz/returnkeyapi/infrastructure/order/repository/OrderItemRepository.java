package com.luthfihariz.returnkeyapi.infrastructure.order.repository;

import com.luthfihariz.returnkeyapi.infrastructure.order.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, String> {

    public List<OrderItem> findByOrderId(String orderId) throws Exception;

}
