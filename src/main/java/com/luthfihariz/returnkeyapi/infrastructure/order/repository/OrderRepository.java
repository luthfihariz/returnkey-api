package com.luthfihariz.returnkeyapi.infrastructure.order.repository;

import com.luthfihariz.returnkeyapi.infrastructure.order.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {
    public Order findByIdAndCustomerEmail(String id, String customerEmail);
}
