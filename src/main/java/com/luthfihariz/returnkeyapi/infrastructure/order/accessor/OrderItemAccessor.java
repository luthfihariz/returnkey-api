package com.luthfihariz.returnkeyapi.infrastructure.order.accessor;

import com.luthfihariz.returnkeyapi.core.order.accessor.IOrderItemAccessor;
import com.luthfihariz.returnkeyapi.core.order.domain.OrderItemDomain;
import com.luthfihariz.returnkeyapi.infrastructure.order.entity.OrderItem;
import com.luthfihariz.returnkeyapi.infrastructure.order.mapper.OrderMapper;
import com.luthfihariz.returnkeyapi.infrastructure.order.repository.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderItemAccessor implements IOrderItemAccessor {
    @Autowired
    OrderItemRepository orderItemRepository;

    @Override
    public List<OrderItemDomain> getByOrderId(String orderId) throws Exception {
        return orderItemRepository.findByOrderId(orderId).stream().map(
                OrderMapper.INSTANCE::orderItemToOrderItemDomain
        ).toList();
    }

    @Override
    public void bulkCreate(List<OrderItemDomain> orderItemDomains) throws Exception {
        List<OrderItem> orderItems = orderItemDomains.stream().map(OrderMapper.INSTANCE::orderItemDomainToOrderItem).toList();
        orderItemRepository.saveAll(orderItems);
    }
}
