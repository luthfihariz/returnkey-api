package com.luthfihariz.returnkeyapi.infrastructure.returns.repository;

import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ReturnTokenRepository extends JpaRepository<ReturnToken, Long> {

    public ReturnToken findByTokenAndExpiredAtAfter(String token, Date now) throws Exception;
}
