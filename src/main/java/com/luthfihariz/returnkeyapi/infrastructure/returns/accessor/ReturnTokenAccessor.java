package com.luthfihariz.returnkeyapi.infrastructure.returns.accessor;

import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnTokenAccessor;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnTokenDomain;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnToken;
import com.luthfihariz.returnkeyapi.infrastructure.returns.mapper.ReturnMapper;
import com.luthfihariz.returnkeyapi.infrastructure.returns.repository.ReturnTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
public class ReturnTokenAccessor implements IReturnTokenAccessor {

    @Autowired
    private ReturnTokenRepository returnTokenRepository;

    @Override
    public ReturnTokenDomain create(ReturnTokenDomain returnTokenDomain) {
        ReturnToken returnToken = ReturnMapper.INSTANCE.returnTokenDomainToReturnToken(returnTokenDomain);
        returnTokenRepository.save(returnToken);

        return ReturnMapper.INSTANCE.returnTokenToReturnTokenDomain(returnToken);
    }

    @Override
    public ReturnTokenDomain getActiveToken(String token) throws Exception {
        return ReturnMapper.INSTANCE.returnTokenToReturnTokenDomain(
                returnTokenRepository.findByTokenAndExpiredAtAfter(token, Calendar.getInstance().getTime()));
    }
}
