package com.luthfihariz.returnkeyapi.infrastructure.returns.mapper;

import com.luthfihariz.returnkeyapi.core.order.domain.OrderDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnTokenDomain;
import com.luthfihariz.returnkeyapi.infrastructure.order.entity.Order;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.Return;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnItem;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnToken;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ReturnMapper {
    ReturnMapper INSTANCE = Mappers.getMapper(ReturnMapper.class);

    ReturnDomain returnToReturnDomain(Return returns);

    Return returnDomainToReturn(ReturnDomain returnDomain);

    ReturnTokenDomain returnTokenToReturnTokenDomain(ReturnToken returnToken);

    ReturnToken returnTokenDomainToReturnToken(ReturnTokenDomain returnTokenDomain);

    ReturnItemDomain returnItemToReturnItemDomain(ReturnItem returnItem);

    ReturnItem returnItemDomainToReturnItem(ReturnItemDomain returnItemDomain);

    Order orderDomainToOrder(OrderDomain orderDomain);
}
