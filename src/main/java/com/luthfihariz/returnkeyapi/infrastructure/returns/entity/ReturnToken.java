package com.luthfihariz.returnkeyapi.infrastructure.returns.entity;

import com.luthfihariz.returnkeyapi.infrastructure.order.entity.Order;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(indexes = @Index(columnList = "token", unique = true))
public class ReturnToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;

    private Date expiredAt;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
}
