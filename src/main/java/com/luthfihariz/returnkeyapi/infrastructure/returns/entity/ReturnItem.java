package com.luthfihariz.returnkeyapi.infrastructure.returns.entity;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemState;
import com.luthfihariz.returnkeyapi.infrastructure.order.entity.OrderItem;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class ReturnItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "return_id")
    private Return returns;

    @ManyToOne
    @JoinColumn(name = "order_item_id")
    private OrderItem orderItem;

    private Integer acceptedQuantity;

    private ReturnItemState status;
}
