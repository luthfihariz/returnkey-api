package com.luthfihariz.returnkeyapi.infrastructure.returns.accessor;

import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnAccessor;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnState;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.Return;
import com.luthfihariz.returnkeyapi.infrastructure.returns.mapper.ReturnMapper;
import com.luthfihariz.returnkeyapi.infrastructure.returns.repository.ReturnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReturnAccessor implements IReturnAccessor {

    @Autowired
    private ReturnRepository returnRepository;

    @Override
    public ReturnDomain create(ReturnDomain returnDomain) throws Exception {
        return ReturnMapper.INSTANCE.returnToReturnDomain(
                returnRepository.save(
                        ReturnMapper.INSTANCE.returnDomainToReturn(returnDomain)));
    }

    @Override
    public ReturnDomain getById(Long id) throws Exception {
        return ReturnMapper.INSTANCE.returnToReturnDomain(
                returnRepository.findById(id).orElse(null)
        );
    }

    @Override
    public ReturnDomain getByOrderId(String orderId) throws Exception {
        return ReturnMapper.INSTANCE.returnToReturnDomain(
                returnRepository.findByOrderId(orderId)
        );
    }

    @Override
    public ReturnDomain updateStatusAndEstimatedPrice(Long id, ReturnState status, Double price) throws Exception {
        Return returns = returnRepository.findById(id).orElse(null);
        if (returns == null) {
            return null;
        }

        returns.setStatus(status);
        returns.setEstimatedAmount(price);
        return ReturnMapper.INSTANCE.returnToReturnDomain(
                returnRepository.save(returns)
        );
    }
}
