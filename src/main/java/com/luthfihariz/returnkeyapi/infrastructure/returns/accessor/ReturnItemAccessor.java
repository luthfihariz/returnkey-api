package com.luthfihariz.returnkeyapi.infrastructure.returns.accessor;

import com.luthfihariz.returnkeyapi.core.returns.accessor.IReturnItemAccessor;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemDomain;
import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemState;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnItem;
import com.luthfihariz.returnkeyapi.infrastructure.returns.mapper.ReturnMapper;
import com.luthfihariz.returnkeyapi.infrastructure.returns.repository.ReturnItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReturnItemAccessor implements IReturnItemAccessor {
    @Autowired
    private ReturnItemRepository returnItemRepository;

    @Override
    public ReturnItemDomain getById(Long id) throws Exception {
        return ReturnMapper.INSTANCE.returnItemToReturnItemDomain(
                returnItemRepository.findById(id).orElse(null)
        );
    }

    @Override
    public ReturnItemDomain updateStatusAndAcceptedQuantity(Long id, ReturnItemState returnItemState, Integer acceptedQuantity) throws Exception {
        ReturnItem returnItem = returnItemRepository.findById(id).orElse(null);
        if (returnItem == null) {
            return null;
        }

        returnItem.setStatus(returnItemState);
        returnItem.setAcceptedQuantity(acceptedQuantity);
        return ReturnMapper.INSTANCE.returnItemToReturnItemDomain(returnItemRepository.save(returnItem));
    }

    @Override
    public void bulkCreate(List<ReturnItemDomain> returnItemDomains) throws Exception {
        returnItemRepository.saveAll(returnItemDomains.stream().map(
                ReturnMapper.INSTANCE::returnItemDomainToReturnItem
        ).toList());
    }

    @Override
    public List<ReturnItemDomain> getByReturnId(Long returnId) throws Exception {
        return returnItemRepository.findByReturnsId(
                returnId
        ).stream().map(ReturnMapper.INSTANCE::returnItemToReturnItemDomain).toList();
    }

    @Override
    public List<ReturnItemDomain> getNonRejectedByReturnId(Long returnId) throws Exception {
        return returnItemRepository.findByReturnsIdAndStatusNot(
                returnId,
                ReturnItemState.REJECTED
        ).stream().map(ReturnMapper.INSTANCE::returnItemToReturnItemDomain).toList();
    }
}
