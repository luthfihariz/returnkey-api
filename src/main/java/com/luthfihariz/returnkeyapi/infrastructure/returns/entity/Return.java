package com.luthfihariz.returnkeyapi.infrastructure.returns.entity;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnState;
import com.luthfihariz.returnkeyapi.infrastructure.order.entity.Order;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Return {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    private Double estimatedAmount;

    private ReturnState status;

    @OneToMany(mappedBy = "returns")
    private Set<ReturnItem> returnItems;
}
