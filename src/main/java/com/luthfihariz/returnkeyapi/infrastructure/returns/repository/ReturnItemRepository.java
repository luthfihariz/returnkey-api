package com.luthfihariz.returnkeyapi.infrastructure.returns.repository;

import com.luthfihariz.returnkeyapi.core.returns.domain.ReturnItemState;
import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.ReturnItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReturnItemRepository extends JpaRepository<ReturnItem, Long> {
    public List<ReturnItem> findByReturnsIdAndStatusNot(Long returnId, ReturnItemState returnItemState) throws Exception;

    public List<ReturnItem> findByReturnsId(Long returnId) throws Exception;
}
