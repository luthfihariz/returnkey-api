package com.luthfihariz.returnkeyapi.infrastructure.returns.repository;

import com.luthfihariz.returnkeyapi.infrastructure.returns.entity.Return;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReturnRepository extends JpaRepository<Return, Long> {
    public Return findByOrderId(String orderId) throws Exception;
}
